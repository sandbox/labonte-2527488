<?php
// $Id:$

/**
 * @file
 * Module's admin settings.
 */

use publicplan\wss\edmond\Service;

/**
 * Callback function for EDMOND specific administration settings.
 *
 * @return array
 *   Renderable drupal settings form.
 */
function wss_edmond_admin_settings() {
  $settings = array();

  $settings[Service::SETTING__ACTIVATED] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate EDMOND search service'),
    '#default_value' => Service::getSetting(
        Service::SETTING__ACTIVATED),
    '#weight' => 10,
  );

  $settings[Service::SETTING__SEARCH_URI] = array(
    '#type' => 'textfield',
    '#title' => t('Edmond/Arix service address'),
    '#description' => 'Syntax: http://edmond.example.com/arix/',
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
        Service::SETTING__SEARCH_URI),
    '#weight' => 20,
  );

  $settings[Service::SETTING__ROOT_IDENTITY] = array(
    '#type' => 'textfield',
    '#title' => t('Edmond/Arix root identity'),
    '#description' => t("Default value: 'NRW'."),
    '#size' => 30,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
        Service::SETTING__ROOT_IDENTITY),
    '#weight' => 30,
  );

  $settings[Service::SETTING__PASSWORD] = array(
    '#type' => 'textfield',
    '#title' => t('Authentication secret'),
    '#size' => 30,
    '#maxlength' => 255,
    '#required' => FALSE,
    '#default_value' => Service::getSetting(
        Service::SETTING__PASSWORD),
    '#weight' => 40,
  );

  $settings[Service::SETTING__MAX_RESULTS] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum result count'),
    '#description' => t('Maximum number of results to be displayed. <br/> Be aware of the visual appearance, when you change this setting.'),
    '#size' => 3,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(
        Service::SETTING__MAX_RESULTS),
    '#weight' => 50,
  );

  $settings[Service::SETTING__LOGO] = array(
    '#type' => 'textarea',
    '#title' => t('Edmond logo'),
    '#description' => t('Here you can alter the HTML snippet retrieved per API to display as logo.'),
    '#rows' => 4,
    '#required' => FALSE,
    '#default_value' => Service::getSetting(
        Service::SETTING__LOGO),
    '#prefix' => '<div id="textarea-edmond-logo" class="clearfix"><div>',
    '#suffix' => '</div><div><label>' . t('Logo preview') . '</label><div>' .
      Service::getSetting(Service::SETTING__LOGO) .
      '</div></div></div>',
    '#weight' => 60,
  );

  $settings['fetch_logo'] = array(
    '#type' => 'submit',
    '#title' => t('Fetch logo(s) from Edmond/Arix service'),
    '#description' => t('Fetches/Updates logo(s) from Edmond/Arix service.'),
    '#submit' => array('wss_edmond_admin_fetch_logo'),
    '#value' => t('Fetch logo via API'),
    '#weight' => 70,
    '#parent' => $settings[Service::SETTING__LOGO],
  );

  $settings['#submit'] = array('wss_edmond_admin_settings_submit');

  return system_settings_form($settings);
}

/**
 * Submit callback for fetching the EDMOND logo via webservice.
 */
function wss_edmond_admin_fetch_logo($form, &$form_state) {
  Service::getInstance()->fetchLogo();
}

/**
 * Default submit callback for EDMOND settings.
 */
function wss_edmond_admin_settings_submit($form, &$form_state) {
  if ($form_state['values'][Service::SETTING__LOGO] !== variable_get(Service::SETTING__LOGO)) {
    $form_state['values'][Service::SETTING__LOGO] = strip_tags(
      $form_state['values'][Service::SETTING__LOGO], '<img><a>'
    );
  }
  if (isset($form_state['values']) && !empty($form_state['values'])) {
    cache_clear_all();
    drupal_set_message('Cleared all caches.');
  }
}
