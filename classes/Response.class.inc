<?php
// $id:$

/**
 * @file
 * This file contains the SODIS specific implementation of the \SearchResponse
 * interface.
 */

namespace publicplan\wss\edmond;

use publicplan\wss\base\SearchResponse;

class Response extends SearchResponse {
  protected $service = Service::NAME;

  /**
   * Parse common response data.
   *
   * @return \publicplan\wss\edmond\Response
   *   Returns itself.
   */
  protected function parseResponseData() {
    // Parse XML string to SimpleXMLElement:
    libxml_use_internal_errors(TRUE);
    $this->raw = simplexml_load_string($this->body);
    if (libxml_get_last_error() || !isset($this->raw)) {
      throw new \RuntimeException(
          t('Underlying search service responded with invalid data portion.'));
    }
    libxml_use_internal_errors();

    // Extract common response data:
    $this->data['total'] = count($this->raw->children());
    $this->getEdmondLocation();
    $root_identity = Service::getSetting(Service::SETTING__ROOT_IDENTITY);
    $add_root_identity = function($location) use ($root_identity) {
      return $root_identity . '/' . $location;
    };
    $this->data['identities'] = array_map($add_root_identity, $this->data['locations']);
    $this->data['primary_identity'] = reset($this->data['identities']);

    return $this;
  }

  /**
   * Extract iterable resultset.
   *
   * @return array
   *   Iterable results.
   * @throws \RuntimeException
   */
  protected function extractResults() {
    if (!($this->raw instanceof \SimpleXMLElement)) {
      throw new \RuntimeException(
          t('Got strange data portion for a SODIS response.'));
    }

    $array = array();
    foreach ($this->raw->children() as $child) {
      $array[] = $child;
    }

    return $array;
  }

  /**
   * Return the EDMOND location.
   *
   * @return string
   *   Location part of the EDMOND identity or if not set an empty string.
   */
  public function getEdmondLocation() {
    if (!isset($this->data['primary_location'])) {
      $this->data['locations'] = filter_input(INPUT_GET, Service::PARAM__LOCATION, FILTER_DEFAULT, FILTER_REQUIRE_ARRAY) ?: array();
      $this->data['primary_location'] = (string) reset($this->data['locations']);
    }

    return $this->data['primary_location'];
  }
}
