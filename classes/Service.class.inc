<?php

// $id:$

/**
 * @file
 * EdmondSearchService implementation.
 */

namespace publicplan\wss\edmond;

use publicplan\wss\base\SearchService;

class Service extends SearchService {
  /**
   * Unique WSS identifier.
   */
  const NAME = 'edmond';

  const PARAM__SEARCH = 'search';

  const PARAM__LOCATION = 'location';

  /**
   * Configuration variable names.
   */
  const SETTING__ACTIVATED = 'wss_edmond_settings__search';
  const SETTING__ROOT_IDENTITY = 'wss_edmond_settings__root_identity';
  const SETTING__PASSWORD = 'wss_edmond_settings__password';
  const SETTING__MAX_RESULTS = 'wss_edmond_settings__max_results';
  const SETTING__SEARCH_URI = 'wss_edmond_settings__search_uri';
  const SETTING__LOGO = 'wss_edmond_settings__logo';

  protected static $identity;

  /**
   * Associative array containing setting identifiers and default values.
   *
   * Array keys are internally used with Drupal's variable_*() functions.
   *
   * @var array
   */
  public static function getDefaultSettings() {
    return array(
      self::SETTING__ACTIVATED => NULL,
      self::SETTING__ROOT_IDENTITY => NULL,
      self::SETTING__PASSWORD => NULL,
      self::SETTING__MAX_RESULTS => 10,
      self::SETTING__SEARCH_URI => NULL,
      self::SETTING__LOGO => NULL,
    );
  }

  /**
   * Implements SearchService->getResourceLocation().
   * @todo Either finish this method implementation or remove it...
   */
  public static function getResourceLocation($resource_id) {
    try {
      $url = Request::create()->formatQueryUrl(drupal_get_query_parameters());

      $xml = new \XMLWriter();
      $xml->openMemory();
      // OPEN root element:
      $xml->startElement('record');
      $xml->writeAttribute('identifier', $resource_id);
      $xml->writeAttribute('template', 'edmond_superplain');
      // CLOSE root element:
      $xml->endElement();

      $response = drupal_http_request($url, array(
        'method' => 'POST',
        'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
        'data' => 'xmlstatement=' . urlencode($xml->outputMemory(FALSE)),
      ));

      return $response->data;
    }
    catch (Exception $e) {
      drupal_set_message($e->getMessage(), 'error');
    }
  }
}
