<?php
// $id:$

/**
 * @file
 * This file contains the Request class interface.
 */

namespace publicplan\wss\edmond;

use publicplan\wss\base\SearchRequest;

class Request extends SearchRequest {
  /**
   * EDMOND request/response cache.
   */
  const CACHE = 'cache_wss_edmond';

  /**
   * Service identifier.
   *
   * @var string
   */
  protected $service = Service::NAME;

  /**
   * HTTP request method.
   *
   * @var string
   */
  protected $method = self::METHOD__POST;

  /**
   * HTTP request headers.
   *
   * @var array
   */
  protected $headers = array(
    'Content-Type' => 'application/x-www-form-urlencoded',
  );

  /**
   * Stores parameters in case of calling $this->setParameters().
   *
   * @var array
   */
  protected $parameters = array();

  /**
   * Assign query parameters.
   *
   * @param array $parameters
   *   Parameter array.
   *
   * @return \publicplan\wss\edmond\Request
   *   Returns itself.
   */
  public function setParameters($parameters) {
    $this->parameters = $parameters;
    $request_body = 'xmlstatement=' . \urlencode($this->formatQueryData($parameters));
    $this->setUrl($this->formatQueryUrl())->setRawData($request_body);

    return $this;
  }

  /**
   * Assign parameters to request licensing information.
   *
   * @param array $parameters
   *   Parameter array.
   * @param array $ids
   *   Array of EDMOND IDs.
   *
   * @return \publicplan\wss\edmond\Request
   *   Returns itself.
   */
  public function setParametersForLicenseRequest($parameters, $ids) {
    $this->parameters = $parameters;
    // Instanciate XMLWriter:
    $xml = new \XMLWriter();
    $xml->openMemory();
    // Write root element:
    $xml->startElement('search');
    $xml->writeAttribute('noliz', '1');
    // OPEN child element `condition`:
    $xml->startElement('condition');
    $xml->writeAttribute('operator', 'OR');
    $xml->writeAttribute('option', 'WORD');
    $xml->writeAttribute('field', 'nr');
    $xml->writeRaw(implode(' ', $ids));
    // CLOSE child elementm `condition`:
    $xml->endElement();
    // CLOSE root element:
    $xml->endElement();

    $this->setUrl($this->formatQueryUrl($parameters))
      ->setRawData('xmlstatement=' . \urlencode($xml->outputMemory(FALSE)));

    return $this;
  }

  /**
   * Return parameters.
   *
   * @return array
   *   Parameter array.
   */
  public function getParameters() {
    return $this->parameters;
  }

  /**
   * Formats the URL for our XML query to stage.
   *
   * @param array $parameters
   *   Query parameter array.
   *
   * @return string
   *   The request URI including the FQDN.
   */
  public function formatQueryUrl($parameters = NULL) {
    if (!$parameters) {
      $parameters = $this->parameters;
    }
    // Determine context/identities:
    $identities = array();
    $root_identity = Service::getSetting(Service::SETTING__ROOT_IDENTITY);
    if (!isset($parameters[Service::PARAM__LOCATION]) || empty($parameters[Service::PARAM__LOCATION])) {
      $identities[] = $root_identity;
    }
    else {
      if (!\is_array($parameters[Service::PARAM__LOCATION])) {
        $parameters[Service::PARAM__LOCATION] = array($parameters[Service::PARAM__LOCATION]);
      }
      foreach ($parameters[Service::PARAM__LOCATION] as $identity) {
        $identities[] = $root_identity . '/' . $identity;
      }
    }
    // Read base URL from settings:
    $base_url = Service::getSetting(Service::SETTING__SEARCH_URI);
    // Ensure trailing slash:
    if (!\substr(Service::getSetting(Service::SETTING__SEARCH_URI), -1) === '/') {
      $base_url .= '/';
    }
    // Concat base URL and identities:
    $url = $base_url . \implode(',', $identities);

    return $url;
  }

  /**
   * Format XML query string according to the given request parameters.
   *
   * @param array $parameters
   *   Query parameter array.
   *
   * @return string
   *   The XML formatted query statement.
   */
  public function formatQueryData($parameters) {
    $searchword = isset($parameters[Service::PARAM__SEARCH]) ?
      (string) $parameters[Service::PARAM__SEARCH] : '';
    // Prepare search term.
    // As long as EDMOND won't accept HTML-Entity-, URL- or Un-encoded special
    // chars like '<' and '>', we simply have to strip off these characters.
    $searchword = str_replace(array('<', '>'), '', $searchword);
    // Instanciate XMLWriter:
    $xml = new \XMLWriter();
    $xml->openMemory();
    // OPEN root element:
    $xml->startElement('search');
    $xml->writeAttribute('fields', 'nr,titel,text,typ,prodjahr,herausgeber');
    if (!empty($searchword)) {
      // OPEN child element `condition`:
      $xml->startElement('condition');
      $xml->writeAttribute('field', 'text_fields');
      $xml->writeRaw($searchword);
      // CLOSE child element `condition`:
      $xml->endElement();
    }
    // CLOSE root element:
    $xml->endElement();

    return $xml->outputMemory(FALSE);
  }

  /**
   * Return the current identity.
   *
   * @return string
   *   EDMOND identity string.
   */
  public function getIdentity() {
    return $this->identity;
  }

  /**
   * Set body data.
   *
   * @param string $plain_data_string
   *   Request body.
   *
   * @return \publicplan\wss\edmond\Request
   *   Returns itself.
   */
  public function setData($plain_data_string) {
    $this->body = $this->body_data = $plain_data_string;
    return $this;
  }
}
