<?php
// $id:$

/**
 * @file
 * This file extends the SearchResultBase class to a SODIS specific form.
 */

namespace publicplan\wss\edmond;

use publicplan\wss\base\SearchResult;

class Result extends SearchResult {
  /**
   * Service identifier.
   *
   * @var string
   */
  protected $service = Service::NAME;

  /**
   * Associative array representing the structure of the formatted array.
   *
   * @var array
   */
  protected $formatStructure = array(
    'nr' => 'nr',
    'titel' => 'title',
    'text' => 'text',
    'typ' => 'type',
    'herausgeber' => 'publisher',
    'prodjahr' => 'year',
  );

  protected $formatCallbacks = array(
    'type' => 'self::type',
  );

  /**
   * Field mappings.
   * Used to translate the cryptic integer representation of some data fields.
   *
   * @var array
   */
  public static  $fieldmap = array(
    'typ' => array(
      29 => 'Audio',
      49 => 'Video',
      55 => 'Scorm',
      19 => 'Bild',
      79 => 'Dokument',
      58 => 'Internetseite',
      69 => 'Programm',
    ),
  );

  /**
   * Parse result data.
   *
   * @param array $raw
   *   Optional raw data array. If omitted, $this->raw will be used.
   *
   * @return \publicplan\wss\sodis\Result
   *   Returns iteself.
   */
  public function parse($raw) {
    $this->raw = $raw;
    // Extract data defined via $this->format_structure.
    foreach ($raw as $element) {
      $attribute = (string) $element['n'];
      $value = (string) $element;
      if (isset($this->formatStructure[$attribute])) {
        $var = $this->formatStructure[$attribute];
        $str_val = isset($this->formatCallbacks[$var]) && is_callable($this->formatCallbacks[$var]) ?
          call_user_func($this->formatCallbacks[$var], $value) : (string) $value;
        $this->data[$var] = $str_val;
      }
    }
    // Extract ID:
    $this->data['id'] = isset($this->raw['identifier']) ?
      (string) $this->raw['identifier'] : $this->data['nr'];
    // Set hyperlink to resource:
    $this->data['href'] = $this->getResourceLocation();

    return $this;
  }

  /**
   * Return the resource location/URI as string.
   *
   * @return string
   *   EDMOND URL for the given resource.
   */
  public function getResourceLocation() {
    $base_url = Service::getSetting(Service::SETTING__SEARCH_URI);
    if (substr($base_url, -1) !== '/') {
      $base_url .= '/';
    }
    $resource_url = $base_url . 'rec?src=online&rtmpl=edmondlogin&id=' . (string) $this->data['id'];
    if (($location = $this->response->getData('primary_location'))) {
      $resource_url .= '&standort=' . $location;
    }

    return $resource_url;
  }

  /**
   * Return filtered copyright values.
   *
   * @param string $input
   *   String to be filtered.
   *
   * @return string
   *   Returns the filtered string.
   */
  public static function type($input) {
    $int = (int) $input;
    return isset(self::$fieldmap['typ'][$int]) ?
      self::$fieldmap['typ'][$int] : '';
  }

  /**
   * Return the internal identifier of the service provider.
   *
   * @return string
   *   EDMOND ID.
   */
  public function getServiceId() {
    return isset($this->data['id']) ? $this->data['id'] : FALSE;
  }
}
